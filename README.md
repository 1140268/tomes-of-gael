# Description
**Tomes (of Gael)** is a 2D action platformer (metroidvania) game set in a fantasy world under threat of an increasingly dangerous foe. 
Our protagonist has in its possession the ability to shift between multiple weapons in rapid succession and deal adaptive blows to enemies through diverse combos.
Knowledge of new weapons is scattered across the world, with challenging mobs, hazardous platforming sections and fierce bosses standing in between.
The goal is to build a unique and comfortable playstyle, choosing from a variety of weapon-exclusive upgrades, in order to better liberate the world.