﻿using System.Collections;
using UnityEngine.UI;
using UnityEngine;

[RequireComponent(typeof(HealthUnit)), RequireComponent(typeof(AttackController))]
public class PlayerAttackController : MonoBehaviour
{

    //Public
    public GameObject weaponW;
    public GameObject weaponA;
    public GameObject weaponS;
    public GameObject weaponD;

    //Private
    private Color32 activeBtnCol = new Color32(73, 172, 255, 200);
    private Color32 deactiveBtnCol = new Color32(255, 255, 255, 200);
    private bool isCharging = false;
    private float chargeStart;
    private Transform physicalPlayer;
    private Rigidbody2D m_rigidbody;
    private CapsuleCollider2D m_collider;
    private PlayerController p_controller;
    private HealthUnit healthCtrl;
    private AttackController attackCtrl;

    // Start is called before the first frame update
    void Awake()
    {
        physicalPlayer = transform.Find("PhysicalPlayer");
        m_rigidbody = GetComponent<Rigidbody2D>();
        m_collider = physicalPlayer.GetComponent<CapsuleCollider2D>();
        p_controller = GetComponent<PlayerController>();
        healthCtrl = GetComponent<HealthUnit>();
        healthCtrl.OnDied += Die;
        healthCtrl.OnTakeDamage += TakeDamage;
        attackCtrl = GetComponent<AttackController>();
        attackCtrl.SetSource(m_rigidbody, m_collider);
        WeaponController.SetEquippedWeapon("A", new Knife());
        WeaponController.SetEquippedWeapon("W", new LightSword());
        WeaponController.SetEquippedWeapon("S", new Bow());
        WeaponController.SetEquippedWeapon("D", new Spear());
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.A) && !isCharging)
        {
            ChargeWeapon();
            weaponA.GetComponent<Image>().color = activeBtnCol;
        }
        else if (Input.GetKeyDown(KeyCode.W) && !isCharging)
        {
            ChargeWeapon();
            weaponW.GetComponent<Image>().color = activeBtnCol;
        }
        else if (Input.GetKeyDown(KeyCode.S) && !isCharging)
        {
            ChargeWeapon();
            weaponS.GetComponent<Image>().color = activeBtnCol;
        }
        else if (Input.GetKeyDown(KeyCode.D) && !isCharging)
        {
            ChargeWeapon();
            weaponD.GetComponent<Image>().color = activeBtnCol;
        }

        if (Input.GetKeyUp(KeyCode.A))
        {
            attackCtrl.PrepareAttack(WeaponController.GetEquippedWeapon("A"), Time.time - chargeStart);
            ResetCharge();
            weaponA.GetComponent<Image>().color = deactiveBtnCol;
        }
        else if (Input.GetKeyUp(KeyCode.W))
        {
            attackCtrl.PrepareAttack(WeaponController.GetEquippedWeapon("W"), Time.time - chargeStart);
            ResetCharge();
            weaponW.GetComponent<Image>().color = deactiveBtnCol;
        }
        else if (Input.GetKeyUp(KeyCode.S))
        {
            attackCtrl.PrepareAttack(WeaponController.GetEquippedWeapon("S"), Time.time - chargeStart);
            ResetCharge();
            weaponS.GetComponent<Image>().color = deactiveBtnCol;
        }
        else if (Input.GetKeyUp(KeyCode.D))
        {
            attackCtrl.PrepareAttack(WeaponController.GetEquippedWeapon("D"), Time.time - chargeStart);
            ResetCharge();
            weaponD.GetComponent<Image>().color = deactiveBtnCol;
        }
        else
        {
            attackCtrl.CheckPauseCombo();
        }
    }

    void ChargeWeapon()
    {
        isCharging = true;
        chargeStart = Time.time;
    }

    void ResetCharge()
    {
        isCharging = false;
        chargeStart = 0;
    } 

    public void Die()
    {
        GetComponent<Collider2D>().enabled = false;
        Destroy(gameObject);
    }

    private void OnDisable()
    {
        healthCtrl.OnDied -= Die;
    }

    private void OnDestroy()
    {
        healthCtrl.OnDied -= Die;        
    }

    private void TakeDamage(float damage, GameObject source, bool isTargetable)
    {
        healthCtrl.DecreaseHealth(damage);
    }
}
