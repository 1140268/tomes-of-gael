﻿using UnityEngine;

public class PlayerController : MonoBehaviour
{
    //**Public**
    public float moveSpeed = 400f;
    public float jumpForce = 700f;
    public float slideSpeed = 0.5f;


    //**Private**
    [SerializeField] private LayerMask enemyMask;
    [SerializeField] private LayerMask ignoreMask;
    private LayerMask groundMask;
    private Collider2D grounded;
    private float horizontalMovement = 0;
    private Transform physicalPlayer;
    private Rigidbody2D m_rigidbody;
    private CapsuleCollider2D m_collider;
    private Transform colliderTransform;
    private bool _jump = false;
    private bool _sliding = false;
    private bool _touchingWall = false;
    private bool _isStuck = false;
    private StatusManager statusManager = new StatusManager();
    private StickySituation stickySituation;
    private Transform originalParent;
    private HealthUnit healthCtrl;
    private GroundCheck groundCheck;

    void Awake()
    {
        physicalPlayer = transform.Find("PhysicalPlayer");
        originalParent = transform.parent;
        m_rigidbody = GetComponent<Rigidbody2D>();
        healthCtrl = GetComponent<HealthUnit>();
        healthCtrl.OnStatusAdded += AddStatus;
        m_collider = physicalPlayer.GetComponent<CapsuleCollider2D>();
        groundCheck = physicalPlayer.GetComponent<GroundCheck>();
        groundMask = groundCheck.groundMask;
        colliderTransform = m_collider.gameObject.transform;
        Physics2D.IgnoreCollision(m_collider, transform.Find("PushCancelling").GetComponent<Collider2D>(), true);
    }

    private void AddStatus(Status status)
    {
        statusManager.AddStatus(status);
    }

    void Update()
    {
        grounded = groundCheck.check();
        if (grounded && grounded.CompareTag("Moving Platform") && transform.parent != grounded.transform.parent)
        {
            m_rigidbody.interpolation = RigidbodyInterpolation2D.None;
            transform.SetParent(grounded.transform.parent);
        }

        if (!grounded && transform.parent != originalParent)
        {
            m_rigidbody.interpolation = RigidbodyInterpolation2D.Interpolate;
            transform.SetParent(originalParent);
        }
        statusManager.Tick();
        if (!statusManager.IsBlocking())
        {
            if (_isStuck)
            {
                if (stickySituation.BreakFree())
                {
                    _isStuck = false;
                }
            }
            else
            {
                ControlMove();
            }
        }
    }

    private void FixedUpdate()
    {
        if (!statusManager.IsBlocking())
        {
            float addedSpeed = horizontalMovement * Time.deltaTime;
            float newSpeed = !grounded && Mathf.Abs(m_rigidbody.velocity.x) > Mathf.RoundToInt(moveSpeed * Time.deltaTime) ? m_rigidbody.velocity.x : addedSpeed;
            m_rigidbody.velocity = new Vector2(_touchingWall ? 0 : newSpeed, m_rigidbody.velocity.y * (_sliding ? slideSpeed : 1));
            if (_jump)
            {
                m_rigidbody.AddForce(Vector2.up * jumpForce);
                _jump = false;
            }
        }
            m_rigidbody.velocity = new Vector2(m_rigidbody.velocity.x * Mathf.Clamp01(1 - 0.1f * Time.fixedDeltaTime), m_rigidbody.velocity.y);
    }

    public void GetStuck()
    {
        if (!_isStuck)
        {
            horizontalMovement = 0;
            _isStuck = true;
            stickySituation = new StickySituation();
        }
    }    

    private void ControlMove()
    {        

        if (Input.GetKeyDown(KeyCode.Space) && grounded)
        {
            _jump = true;
        }


        if (Input.GetKey(KeyCode.LeftArrow) && !Input.GetKey(KeyCode.RightArrow))
        {
            if (transform.eulerAngles.y == 0)
            {
                transform.eulerAngles = new Vector3(transform.eulerAngles.x, 180, transform.eulerAngles.z);
            }
            horizontalMovement = -moveSpeed;
        }

        if (!Input.GetKey(KeyCode.LeftArrow) && Input.GetKey(KeyCode.RightArrow))
        {
            if (transform.eulerAngles.y == 180)
            {
                transform.eulerAngles = new Vector3(transform.eulerAngles.x, 0, transform.eulerAngles.z);
            }
            horizontalMovement = moveSpeed;
        }

        if (!Input.GetKey(KeyCode.LeftArrow) && !Input.GetKey(KeyCode.RightArrow))
        {
            _sliding = false;
            horizontalMovement = 0;
        }
        else
        {
            CheckWallTouch(Mathf.Sign(horizontalMovement));
            ApplyWallSlide(Mathf.Sign(horizontalMovement));
        }

        if (Physics2D.OverlapBox(new Vector2(colliderTransform.position.x + (m_collider.bounds.extents.x + 0.03f) * Mathf.Sign(horizontalMovement), colliderTransform.position.y), new Vector2(0.01f, m_collider.bounds.extents.y * 1.9f), 0, enemyMask))
        {
            horizontalMovement = 0;
        }
    }


    private void CheckWallTouch(float direction)
    {
        Collider2D touchCollision = Physics2D.OverlapBox(new Vector2(colliderTransform.position.x + (m_collider.bounds.extents.x * direction), colliderTransform.position.y), new Vector2(0.02f, m_collider.bounds.extents.y * 2), 0, ignoreMask);
        if (!grounded && touchCollision && !touchCollision.isTrigger)
        {
            _touchingWall = true;
        }
        else
        {
            _touchingWall = false;
        }
    }

    private void ApplyWallSlide(float direction)
    {
        Collider2D sideCollision = Physics2D.OverlapBox(new Vector2(colliderTransform.position.x + (m_collider.bounds.extents.x * direction), colliderTransform.position.y - m_collider.bounds.extents.y / 2), new Vector2(0.01f, m_collider.bounds.extents.y), 0, groundMask);
        if (!grounded && sideCollision && !sideCollision.isTrigger)
        {
            _sliding = true;
        }
        else
        {
            _sliding = false;
        }
    }
}
