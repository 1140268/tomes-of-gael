﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoamPassive : MonoBehaviour
{
    [SerializeField] private bool isPatrolling;
    [SerializeField] private float moveSpeed;
    [SerializeField] private float jumpForce;
    [SerializeField] private float patrolDistance;
    private Rigidbody2D m_rigidbody;
    private float horizontalMovement;
    private Vector3 startPosition;

    private void Awake()
    {
        m_rigidbody = GetComponent<Rigidbody2D>();
        startPosition = transform.position;
        Physics2D.IgnoreCollision(transform.Find("Collider").GetComponent<Collider2D>(), transform.Find("PushAndDamage").GetComponent<Collider2D>(), true);
    }

    void Update()
    {
        if (isPatrolling)
        {
            if (horizontalMovement == 0)
            {
                horizontalMovement = moveSpeed;
            }
            if ((m_rigidbody.position.x - startPosition.x >= patrolDistance && horizontalMovement > 0) || (m_rigidbody.position.x - startPosition.x <= -patrolDistance && horizontalMovement < 0))
            {
                horizontalMovement *= -1;
                transform.eulerAngles = new Vector3(transform.eulerAngles.x, Mathf.Sign(horizontalMovement) > 0 ? 0 : 180, transform.eulerAngles.z);
            }
        }
    }

    private void FixedUpdate()
    {
        m_rigidbody.velocity = new Vector2(horizontalMovement, m_rigidbody.velocity.y);
    }
}
