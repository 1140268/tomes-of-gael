﻿

using UnityEngine;

public class BasicEngage : IState
{
    public readonly EnemyLevel enemy;
    private GameObject target;
    private float movingDirection;
    public BasicEngage(EnemyLevel enemy, GameObject target)
    {
        this.enemy = enemy;
        this.target = target;
    }

    public void SetTarget(GameObject target)
    {
        this.target = target;
    }

    public void OnEnter()
    {
        movingDirection = enemy.GetDirectionToTarget(target);
    }

    public void OnExit()
    {
    }

    public void Tick()
    {
        DirectionToLastTargetSighting();
        ChangeDirection();
        EngagementMovement();
    }

    private void EngagementMovement()
    {
        
        Collider2D groundInRange = Physics2D.OverlapBox(new Vector2(enemy.m_rigidbody.position.x + (enemy.m_collider.bounds.extents.x + 0.5f) * movingDirection, enemy.m_rigidbody.position.y), new Vector2(1, enemy.m_collider.bounds.extents.y * 1.9f), 0, enemy.groundMask);
        if (groundInRange)
        {
            enemy.Jump();
        }
        enemy.horizontalMovement = enemy.moveSpeed * movingDirection;

        if (enemy.IsInSafeDistanceOfTarget() || Physics2D.OverlapBox(new Vector2(enemy.m_rigidbody.position.x + (enemy.m_collider.bounds.extents.x + 0.03f) * movingDirection, enemy.m_rigidbody.position.y), new Vector2(0.01f, enemy.m_collider.bounds.extents.y * 1.9f), 0, enemy.ignoreMask))
        {
            enemy.horizontalMovement = 0;
        }

        if (enemy.IsTooCloseToTarget(out float direction))
        {
            enemy.horizontalMovement = enemy.moveSpeed * direction;
        }
    }

    private void DirectionToLastTargetSighting()
    {
        RaycastHit2D isInView = Physics2D.Raycast(enemy.transform.position, (target.transform.position - enemy.transform.position).normalized, 1000, enemy.groundMask | 1 << target.layer);
        if(isInView && isInView.transform.CompareTag(target.tag))
        {
            movingDirection = Mathf.Sign(isInView.point.x - enemy.transform.position.x);
        }
    }

    private void ChangeDirection()
    {
        if (movingDirection == -1 && enemy.transform.eulerAngles.y == 0)
        {
            enemy.transform.eulerAngles = new Vector3(enemy.transform.eulerAngles.x, 180, enemy.transform.eulerAngles.z);
        }
        if (movingDirection == 1 && enemy.transform.eulerAngles.y == 180)
        {
            enemy.transform.eulerAngles = new Vector3(enemy.transform.eulerAngles.x, 0, enemy.transform.eulerAngles.z);
        }
    }
}

