﻿using UnityEngine;

public class BasicPatrol : IState
{
    public readonly EnemyLevel enemy;
    public bool isPatrolling = false;

    public BasicPatrol(EnemyLevel enemy)
    {
        this.enemy = enemy;
    }

    public void OnEnter()
    {
        isPatrolling = true;
    }

    public void OnExit()
    {
        isPatrolling = false;
    }

    public void Tick()
    {
        if (enemy.horizontalMovement == 0)
        {
            enemy.horizontalMovement = enemy.moveSpeed;
        }
        if ((enemy.m_rigidbody.position.x - enemy.startPosition.x >= enemy.patrolDistance && enemy.horizontalMovement > 0) || (enemy.m_rigidbody.position.x - enemy.startPosition.x <= -enemy.patrolDistance && enemy.horizontalMovement < 0))
        {
            enemy.horizontalMovement *= -1;
            enemy.transform.eulerAngles = new Vector3(enemy.transform.eulerAngles.x, Mathf.Sign(enemy.horizontalMovement) > 0 ? 0 : 180, enemy.transform.eulerAngles.z);
        }
    }
}

