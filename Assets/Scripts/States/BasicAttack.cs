﻿

using UnityEngine;

public class BasicAttack : IState
{
    public readonly EnemyLevel enemy;
    private GameObject target;
    private AttackController attackCtrl;

    public BasicAttack(EnemyLevel enemy, GameObject target, AttackController attackCtrl)
    {
        this.enemy = enemy;
        this.target = target;
        this.attackCtrl = attackCtrl;
    }

    public void SetTarget(GameObject target)
    {
        this.target = target;
    }

    public void OnEnter()
    {
        enemy.horizontalMovement = 0;
    }

    public void OnExit()
    {
    }

    public void Tick()
    {
        enemy.patienceStart = Time.time;
        AttackTarget();
    }

    private void AttackTarget()
    {        
        ChangeDirection();
        if (enemy.IsTooCloseToTarget(out float direction))
        {
            enemy.horizontalMovement = enemy.moveSpeed * direction;
        }
        else
        {
            attackCtrl.PrepareAttack(enemy.weapon, RandomSingleton.getInstance().Next(1, 3) % 2 == 0 ? 0 : float.MaxValue);
        }
    }

    private void ChangeDirection()
    {
        if (target.transform.position.x < enemy.transform.position.x && enemy.transform.eulerAngles.y == 0)
        {
            enemy.transform.eulerAngles = new Vector3(enemy.transform.eulerAngles.x, 180, enemy.transform.eulerAngles.z);
        }
        else if (target.transform.position.x > enemy.transform.position.x && enemy.transform.eulerAngles.y == 180)
        {
            enemy.transform.eulerAngles = new Vector3(enemy.transform.eulerAngles.x, 0, enemy.transform.eulerAngles.z);
        }
    }
}

