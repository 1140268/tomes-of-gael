﻿public class Dead : IState
{
    public readonly HealthUnit objectWithHealth;

    public Dead(HealthUnit objectWithHealth)
    {
        this.objectWithHealth = objectWithHealth;
    }

    public void OnEnter()
    {
        objectWithHealth.Die();
    }

    public void OnExit()
    {
    }

    public void Tick()
    {
    }
}

