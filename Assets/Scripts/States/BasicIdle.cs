﻿using UnityEngine;

public class BasicIdle : IState
{
    public readonly EnemyLevel enemy;

    public BasicIdle(EnemyLevel enemy)
    {
        this.enemy = enemy;
    }

    public void OnEnter()
    {
    }

    public void OnExit()
    {
    }

    public void Tick()
    {
        float movingDirection = Mathf.Sign(enemy.startPosition.x - enemy.transform.position.x);
        if (Vector2.Distance(enemy.transform.position, enemy.startPosition) > 0.1f)
        {
            if (movingDirection == 1)
            {
                enemy.transform.eulerAngles = new Vector3(enemy.transform.eulerAngles.x, 0, enemy.transform.eulerAngles.z);
            }
            if (movingDirection == -1)
            {
                enemy.transform.eulerAngles = new Vector3(enemy.transform.eulerAngles.x, 180, enemy.transform.eulerAngles.z);
            }

            Collider2D groundInRange = Physics2D.OverlapBox(new Vector2(enemy.m_rigidbody.position.x + (enemy.m_collider.bounds.extents.x + 0.5f) * movingDirection, enemy.m_rigidbody.position.y), new Vector2(1, enemy.m_collider.bounds.extents.y * 1.9f), 0, enemy.groundMask);
            if (groundInRange && enemy.transform.position.y + 0.1f * movingDirection < enemy.startPosition.y)
            {
                enemy.Jump();
            }
            enemy.horizontalMovement = enemy.moveSpeed * movingDirection;
        }

        if (Vector2.Distance(enemy.transform.position, enemy.startPosition) <= 0.1f ||
            Physics2D.OverlapBox(new Vector2(enemy.m_rigidbody.position.x + (enemy.m_collider.bounds.extents.x + 0.03f) * movingDirection, enemy.m_rigidbody.position.y), new Vector2(0.01f, enemy.m_collider.bounds.extents.y * 1.9f), 0, enemy.ignoreMask))
        {
            enemy.horizontalMovement = 0;
        }
    }
}

