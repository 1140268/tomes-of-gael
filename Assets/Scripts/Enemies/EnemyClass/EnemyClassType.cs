﻿using UnityEngine;

public class EnemyClassType : MonoBehaviour
{

    public float safeWeaponDist = 2;
    public Weapon EquippedWeapon { set; get; }
    public float Armor { set; get; }

    public Collider2D HitBoxBuilder(Rigidbody2D rigidbody, Collider2D collider, float movingDirection, LayerMask mask)
    {
        float maxX = Mathf.Max(EquippedWeapon.LightSize.x, EquippedWeapon.HeavySize.x);
        float maxY = Mathf.Max(EquippedWeapon.LightSize.y, EquippedWeapon.HeavySize.y);
        Vector2 point = new Vector2(rigidbody.position.x + (collider.bounds.extents.x + maxX / safeWeaponDist) * movingDirection, rigidbody.position.y);
        Vector2 size = new Vector2(maxX, maxY);
        return Physics2D.OverlapBox(point, size, 0, mask);
    }
}
