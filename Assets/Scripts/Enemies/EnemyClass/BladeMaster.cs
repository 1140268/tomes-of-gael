﻿using System.Collections.Generic;
using UnityEngine;

public class BladeMaster : EnemyClassType
{
    [SerializeField] private string weaponName;

    private void Awake()
    {
            List<string> possibleWeapons = new List<string>{ "LightSword", "Knife" };
        if (!weaponName.Equals("") && possibleWeapons.Contains(weaponName))
        {
            EquippedWeapon = WeaponFactory.GetWeaponByName(weaponName);
        }
        else
        {
            int idx = RandomSingleton.getInstance().Next(possibleWeapons.Count);
            EquippedWeapon = WeaponFactory.GetWeaponByName(possibleWeapons[RandomSingleton.getInstance().Next(possibleWeapons.Count)]);
        }
    }
}
