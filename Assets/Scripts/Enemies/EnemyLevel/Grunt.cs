﻿using UnityEngine;

public class Grunt : EnemyLevel, IEnemyLevel
{
    private BasicPatrol basicPatrol;
    private BasicEngage basicEngage;
    private BasicAttack basicAttack;

    protected override void Awake()
    {
        base.Awake();
        Dead dead = new Dead(healthCtrl);
        basicEngage = new BasicEngage(this, player);
        basicAttack = new BasicAttack(this, player, attackCtrl);
        basicPatrol = new BasicPatrol(this);
        stateMachine.AddAnyTransition(dead, healthCtrl.HasDied);
        if (!IsPatrolling)
        {
            BasicIdle basicIdle = new BasicIdle(this);
            stateMachine.AddTransition(basicEngage, basicIdle, () => target == null);
            stateMachine.AddTransition(basicIdle, basicEngage, CanEngageTarget);
            stateMachine.SetState(basicIdle);
        }
        else
        {
            stateMachine.AddTransition(basicEngage, basicPatrol, () => target == null);
            stateMachine.AddTransition(basicPatrol, basicEngage, CanEngageTarget);
            stateMachine.SetState(basicPatrol);
        }
        stateMachine.AddTransition(basicEngage, basicAttack, () => CanAttackTarget(target) != null);
        stateMachine.AddTransition(basicAttack, basicEngage, () => CanAttackTarget(target) == null);
    }

    protected override void Start()
    {
        base.Start();
    }

    protected override void Update()
    {
            base.Update();
    }

    protected override void FixedUpdate()
    {
            base.FixedUpdate();
    }

    public override void EngagePlayer()
    {
        if (target == null || !target.CompareTag("Player"))
        {
            IsPlayerInView(distance);
        }
    }

    public override void SetTarget(GameObject target)
    {
        base.SetTarget(target);
        basicEngage.SetTarget(target);
        basicAttack.SetTarget(target);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (basicPatrol.isPatrolling &&
            collision.transform.CompareTag("Ground") &&
            collision.contacts[0].point.y > m_rigidbody.position.y - m_collider.bounds.extents.y)
        {
            horizontalMovement *= -1;
        }
    }
}
