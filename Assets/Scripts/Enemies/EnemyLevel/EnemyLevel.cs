﻿using System.Collections;
using UnityEngine;

public abstract class EnemyLevel : MonoBehaviour
{
    //Configurable values
    public float moveSpeed = 3f;
    public float jumpForce = 700;
    public float jumpCooldown = 3;
    public float patrolDistance = 2f;
    public float corpseDuration = 5f;
    public float patience = 5;
    public bool IsPatrolling;
    public LayerMask ignoreMask;
    public LayerMask playerMask;

    //Shared
    [HideInInspector] public Rigidbody2D m_rigidbody;
    [HideInInspector] public float horizontalMovement;
    [HideInInspector] public Vector2 startPosition;
    [HideInInspector] public CapsuleCollider2D m_collider;
    [HideInInspector] public GameObject player;
    [HideInInspector] public Weapon weapon;
    [HideInInspector] public Collider2D p_collider;
    [HideInInspector] public EnemyClassType enemyClassType;
    [HideInInspector] public float patienceStart;
    [HideInInspector] public LayerMask groundMask;


    [SerializeField] protected float distance = 7;
    protected Transform physicalEnemy;
    protected float timeToNextJump;
    protected bool _jump = false;
    protected bool _touchingWall = false;
    protected Collider2D grounded;
    protected StateMachine stateMachine;
    protected StatusManager statusManager;
    protected HealthUnit healthCtrl;
    protected GroundCheck groundCheck;
    protected GameObject target;
    protected AttackController attackCtrl;
    protected LayerMask groundPlayerMask;

    private Transform originalParent;

    public abstract void EngagePlayer();

    public void Engage(GameObject source)
    {
        if (source != null && source.GetComponent<HealthUnit>() != null)
        {
            patienceStart = Time.time;
            p_collider = source.transform.GetChild(0).GetComponent<Collider2D>();
            SetTarget(source);
        }
    }

    public void Disengage()
    {
        SetTarget(null);
    }

    public virtual void SetTarget(GameObject target)
    {
        this.target = target;
    }

    public void Jump()
    {
        float currentTime = Time.time;
        if (grounded && currentTime >= timeToNextJump)
        {
            timeToNextJump = currentTime + jumpCooldown;
            _jump = true;
        }
    }

    public float GetDirectionToTarget(GameObject target)
    {
        return Mathf.Sign(target.transform.position.x - transform.position.x);
    }

    public float DistanceToTarget(GameObject target)
    {
        float movingDirection = GetDirectionToTarget(target);
        return Mathf.Abs((target.transform.position.x + (p_collider.bounds.extents.x * -movingDirection)) - (m_collider.transform.position.x + (m_collider.bounds.extents.x * movingDirection)));
    }

    public bool IsInSafeDistanceOfTarget()
    {
        if (weapon.IsProjectile)
        {
            return IsTargetInProjectileRange();
        }
        return IsOutsideMaxWeaponBounds();
    }
    public bool IsTooCloseToTarget(out float direction)
    {
        RaycastHit2D isInView = Physics2D.Raycast(transform.position, (target.transform.position - transform.position).normalized, 3, groundPlayerMask);
        if (!isInView || !CheckIfHitPlayer(isInView))
        {
            direction = 0;
            return false;
        }
        if (transform.position.x >= target.transform.position.x &&
            transform.position.x <= target.transform.position.x + p_collider.bounds.extents.x)
        {
            direction = 1;
            return true;
        }
        if (transform.position.x < target.transform.position.x &&
            transform.position.x >= target.transform.position.x - p_collider.bounds.extents.x)
        {
            direction = -1;
            return true;
        }
        if (transform.position.x + m_collider.bounds.extents.x >= target.transform.position.x - p_collider.bounds.extents.x &&
            transform.position.x - m_collider.bounds.extents.x < target.transform.position.x - p_collider.bounds.extents.x)
        {
            direction = -1;
            return true;
        }
        if (transform.position.x + m_collider.bounds.extents.x > target.transform.position.x + p_collider.bounds.extents.x &&
            transform.position.x - m_collider.bounds.extents.x <= target.transform.position.x + p_collider.bounds.extents.x)
        {
            direction = 1;
            return true;
        }
        float xLimit = (weapon.IsProjectile ? weapon.ProjectileSize.x : Mathf.Min(weapon.LightSize.x, weapon.HeavySize.x)) / enemyClassType.safeWeaponDist / 2;
        direction = -GetDirectionToTarget(target);
        return (DistanceToTarget(target) <= xLimit);
    }

    public Collider2D CanAttackTarget(GameObject target)
    {
        if (weapon.IsProjectile)
        {
            return IsTargetInProjectileRange();
        }
        return enemyClassType.HitBoxBuilder(m_rigidbody, m_collider, GetDirectionToTarget(target), 1 << target.layer);
    }

    protected virtual void Awake()
    {
        originalParent = transform.parent;
        horizontalMovement = 0;

        enemyClassType = GetComponent<EnemyClassType>();

        m_rigidbody = GetComponent<Rigidbody2D>();
        startPosition = m_rigidbody.position;

        physicalEnemy = transform.Find("Physical Enemy");
        m_collider = physicalEnemy.GetComponent<CapsuleCollider2D>();

        stateMachine = new StateMachine();
        statusManager = new StatusManager();

        groundCheck = physicalEnemy.GetComponent<GroundCheck>();
        groundMask = groundCheck.groundMask;
        groundPlayerMask = groundMask | playerMask;

        healthCtrl = GetComponent<HealthUnit>();
        healthCtrl.OnDied += Die;
        healthCtrl.OnStatusAdded += AddStatus;
        healthCtrl.OnTakeDamage += TakeDamage;

        attackCtrl = GetComponent<AttackController>();
        attackCtrl.SetSource(m_rigidbody, m_collider);

        Physics2D.IgnoreCollision(m_collider, transform.Find("PushCancelling").GetComponent<Collider2D>(), true);
    }

    protected virtual void Start()
    {
        player = GameSettings.Player;
        weapon = enemyClassType.EquippedWeapon;
    }

    protected virtual void Update()
    {
            grounded = groundCheck.check();
            if (grounded && grounded.CompareTag("Moving Platform") && transform.parent != grounded.transform.parent)
            {
                m_rigidbody.interpolation = RigidbodyInterpolation2D.None;
                transform.SetParent(grounded.transform.parent);
            }

            if (!grounded && transform.parent != originalParent)
            {
                m_rigidbody.interpolation = RigidbodyInterpolation2D.Interpolate;
                transform.SetParent(originalParent);
            }


            if (target != null && LostPatience())
            {
                Disengage();
            }
            statusManager.Tick();
        if (!statusManager.IsBlocking())
        {
            stateMachine.Tick();
        }
    }

    protected virtual void FixedUpdate()
    {
        if (!statusManager.IsBlocking())
        {
            float addedSpeed = horizontalMovement * Time.deltaTime;
            float newSpeed = !grounded && Mathf.Abs(m_rigidbody.velocity.x) > Mathf.RoundToInt(moveSpeed * Time.deltaTime) ? m_rigidbody.velocity.x : addedSpeed;
            m_rigidbody.velocity = new Vector2(_touchingWall ? 0 : newSpeed, m_rigidbody.velocity.y);
            if (_jump)
            {
                m_rigidbody.AddForce(Vector2.up * jumpForce);
                _jump = false;
            }
        }
        m_rigidbody.velocity = new Vector2(m_rigidbody.velocity.x * Mathf.Clamp01(1 - 0.5f * Time.fixedDeltaTime), m_rigidbody.velocity.y);
    }

    protected void IsPlayerInView(float distance)
    {
        float startX = transform.position.x;
        float startY = transform.position.y;

        RaycastHit2D topCheck = Physics2D.Raycast(new Vector2(startX + m_collider.bounds.extents.x * transform.right.x, startY + m_collider.bounds.extents.y * 0.75f), transform.right, distance, groundPlayerMask);
        RaycastHit2D middleCheck = Physics2D.Raycast(new Vector2(startX + m_collider.bounds.extents.x * transform.right.x, startY), transform.right, distance, groundPlayerMask);
        RaycastHit2D bottomCheck = Physics2D.Raycast(new Vector2(startX + m_collider.bounds.extents.x * transform.right.x, startY - m_collider.bounds.extents.y * 0.75f), transform.right, distance, groundPlayerMask);
        if (CheckIfHitPlayer(topCheck) || CheckIfHitPlayer(middleCheck) || CheckIfHitPlayer(bottomCheck))
        {
            Engage(player);
        }
    }

    protected bool CanEngageTarget()
    {
        return target != null;
    }

    protected bool LostPatience()
    {
        return Time.time - patienceStart >= patience;
    }

    private void AddStatus(Status status)
    {
        statusManager.AddStatus(status);
    }

    private void Die()
    {
        statusManager.ClearStatuses();
        horizontalMovement = 0;
        m_rigidbody.bodyType = RigidbodyType2D.Kinematic;
        m_rigidbody.velocity = Vector2.zero;
        transform.GetChild(2).gameObject.SetActive(false);
        transform.GetChild(0).gameObject.SetActive(false);
        StartCoroutine(Cleanup(corpseDuration));
    }

    private void TakeDamage(float damage, GameObject source, bool isTargetable)
    {
        if (isTargetable)
        {
            Engage(source);
        }
        healthCtrl.DecreaseHealth(damage);
    }

    private IEnumerator Cleanup(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        Destroy(gameObject);
    }

    private bool CheckIfHitPlayer(RaycastHit2D result)
    {
        if (result)
        {
            return result.transform.CompareTag("Player");
        }
        return false;
    }

    private bool IsOutsideMaxWeaponBounds()
    {
        RaycastHit2D result = Physics2D.Raycast(transform.position, (target.transform.position - transform.position).normalized, Vector2.Distance(target.transform.position, transform.position), groundMask | 1 << target.layer);
        if (!result || !CheckIfHitPlayer(result))
            return false;
        float yLimit = Mathf.Max(weapon.LightSize.y, weapon.HeavySize.y) / enemyClassType.safeWeaponDist;
        float xLimit = Mathf.Max(weapon.LightSize.x, weapon.HeavySize.x) / enemyClassType.safeWeaponDist;
        return (DistanceToTarget(target) <= xLimit) && (Mathf.Sign(result.point.y - transform.position.y) > 0 ? result.point.y > transform.position.y + yLimit : result.point.y < transform.position.y + -yLimit);
    }

    private Collider2D IsTargetInProjectileRange()
    {
        LayerMask hitMask = groundMask | (1 << target.layer);
        RaycastHit2D blockCheck = Physics2D.Raycast(transform.position, (target.transform.position - transform.position).normalized, weapon.ProjectileRange + m_collider.bounds.extents.x, hitMask);
        return CheckIfHitPlayer(blockCheck) ? blockCheck.collider : null;
    }

    private void OnDestroy()
    {
        healthCtrl.OnDied -= Die;
        healthCtrl.OnStatusAdded -= AddStatus;
        healthCtrl.OnTakeDamage -= TakeDamage;
    }
}
