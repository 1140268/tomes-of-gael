﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckSurpriseBump : MonoBehaviour
{
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.transform.CompareTag("Player"))
        {
            GetComponentInParent<EnemyLevel>().Engage(collision.gameObject);
        }
    }
}
