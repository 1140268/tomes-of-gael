﻿using System.Collections.Generic;
using UnityEngine;

public class EnemyVision : MonoBehaviour
{
    private EnemyLevel parent;

    private void Start()
    {
        parent = transform.GetComponentInParent<EnemyLevel>();
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            parent.EngagePlayer();
            parent.patienceStart = Time.time;
        }
    }
}
