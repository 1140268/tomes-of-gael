﻿using System;
using UnityEngine;

public class StickySituation
{
    private float escapeInterval;
    private int escapeCount;
    private float nextTick = 0;
    public int SpaceCount { set; get; }

    public StickySituation(int escapeCount = 5, float escapeInterval = 0.3f) 
    {
        this.escapeInterval = escapeInterval;
        this.escapeCount = escapeCount;
    }

    public bool BreakFree()
    {
        if(SpaceCount == 0)
        {
            nextTick = Time.time;
        }

        if (Input.GetKeyDown(KeyCode.Space) && Time.time >= nextTick)
        {
            nextTick = Time.time + escapeInterval;
            SpaceCount++;
        }
        return SpaceCount >= escapeCount;
    }
}