﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraParallax : MonoBehaviour
{
    [SerializeField] private Vector2 parallaxSpeed = new Vector2(0.5f, 0.5f);
    private Transform cameraTransform;
    private Vector3 lastPosition;
    void Start()
    {
        cameraTransform = Camera.main.transform;
        lastPosition = cameraTransform.position;
    }

    void LateUpdate()
    {
        Vector3 deltaMovement = cameraTransform.position - lastPosition;
        transform.position += new Vector3(deltaMovement.x * parallaxSpeed.x, deltaMovement.y * parallaxSpeed.y);
        lastPosition = cameraTransform.position;
    }
}
