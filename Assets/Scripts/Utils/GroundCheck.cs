﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundCheck : MonoBehaviour
{
    public LayerMask groundMask;
    private CapsuleCollider2D m_collider;
    private Vector2 groundCheckPoint = new Vector2();
    private Vector2 groundCheckSize = new Vector2();
    void Awake()
    {
        m_collider = GetComponent<CapsuleCollider2D>();
    }

    public Collider2D check()
    {
        groundCheckPoint.Set(transform.position.x, transform.position.y - m_collider.bounds.extents.y);
        groundCheckSize.Set(m_collider.bounds.extents.x * 1.9f, 0.1f);
        return Physics2D.OverlapBox(groundCheckPoint, groundCheckSize, 0, groundMask);
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireCube(groundCheckPoint, groundCheckSize);
    }
}
