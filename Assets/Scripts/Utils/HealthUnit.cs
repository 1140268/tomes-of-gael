﻿using System;
using UnityEngine;

public class HealthUnit : MonoBehaviour
{
    [SerializeField]
    protected float startingHealth = 100;
    [SerializeField]
    private float health;
    private bool _died;

    public event Action OnDied;
    public event Action<Status> OnStatusAdded;
    public event Action<float, GameObject, bool> OnTakeDamage;

    public bool HasDied() => _died;

    private void Awake()
    {
        _died = false;
        health = startingHealth;
    }

    public void TakeDamage(float damage, GameObject source = null, bool isTargetable = true)
    {
        OnTakeDamage?.Invoke(damage, source, isTargetable);
    }

    public void DecreaseHealth(float damage)
    {
        health -= damage;
        if (health <= 0)
        {
            _died = true;
        }
    }

    public void Die()
    {
        OnDied?.Invoke();
    }
    public void AddStatus(Status status)
    {
        OnStatusAdded?.Invoke(status);
    }
}
