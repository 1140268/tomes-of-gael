﻿using System.Collections;
using UnityEngine;

public class AttackController : MonoBehaviour
{
    [SerializeField]
    protected float attackRefreshTime = 3;
    [SerializeField]
    protected int maxAttacks = 4;
    [SerializeField]
    protected LayerMask enemyMask;

    private int usedAttacks = 0;
    private float timeToNextAttack = 0;
    private float resetAttackTimer;
    private Rigidbody2D m_rigidbody;
    private CapsuleCollider2D m_collider;
    Vector2 size = new Vector2();
    Vector2 point = new Vector2();

    public void SetSource(Rigidbody2D rigidbody, CapsuleCollider2D collider)
    {
        m_rigidbody = rigidbody;
        m_collider = collider;
    }

    public void PrepareAttack(Weapon weapon, float chargeTime)
    {
        float currentTime = Time.time;
        if ((usedAttacks == 0 || currentTime >= timeToNextAttack) && usedAttacks < maxAttacks)
        {
            timeToNextAttack = currentTime + weapon.CastTime;
            DetectAttack(weapon, chargeTime);
            usedAttacks++;
            resetAttackTimer = currentTime + attackRefreshTime;

            if (usedAttacks >= maxAttacks)
            {
                StartCoroutine(WaitForResetAttacks(attackRefreshTime));
            }
        }
    }

    public void CheckPauseCombo()
    {
        if (usedAttacks > 0 && usedAttacks < maxAttacks && Time.time >= resetAttackTimer)
            ResetAttacks();
    }

    IEnumerator WaitForResetAttacks(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        ResetAttacks();
    }

    void ResetAttacks()
    {
        usedAttacks = 0;
        timeToNextAttack = 0;
    }

    void DetectAttack(Weapon weapon, float chargeTime)
    {
        int direction = (transform.eulerAngles.y == 0 ? 1 : -1);
        float attackDmg;
        if (chargeTime < weapon.ChargeTime)
        {
            attackDmg = weapon.LightDmg;
            size = weapon.LightSize;
        }
        else
        {
            attackDmg = weapon.HeavyDmg;
            size = weapon.HeavySize;
        }

        if (weapon.IsProjectile)
        {
            SetProjectileAndFire(weapon, direction, attackDmg);
        }
        else
        {
            point.Set(m_rigidbody.position.x + (m_collider.bounds.extents.x + size.x / 2) * direction, m_rigidbody.position.y);
            if (weapon.MultiHits)
            {
                Collider2D[] attackHits = Physics2D.OverlapBoxAll(point, size, 0, enemyMask);
                foreach (Collider2D attackHit in attackHits)
                {
                    PerformAttack(attackHit, attackDmg);
                }
            }
            else
            {
                Collider2D attackHit = Physics2D.OverlapBox(point, size, 0, enemyMask);
                if (attackHit)
                {
                    PerformAttack(attackHit, attackDmg);
                }
            }

        }

    }

    private void SetProjectileAndFire(Weapon weapon, float direction, float attackDmg)
    {
        GameObject projectile = Instantiate(weapon.ProjectilePrefab);
        projectile.transform.position = new Vector2(m_rigidbody.position.x + (m_collider.bounds.extents.x + weapon.Length / 2) * direction, m_rigidbody.position.y);
        ProjectileController controller = projectile.GetComponent<ProjectileController>();
        controller.origin = transform.gameObject;
        controller.weapon = weapon;
        controller.facingDirection = direction;
        controller.damage = attackDmg;
        controller.enemyMask = enemyMask;
    }

    private void PerformAttack(Collider2D target, float damage)
    {
        HealthUnit healthCtrl = target.transform.GetComponent<HealthUnit>();
        if (healthCtrl == null)
        {
            healthCtrl = target.transform.parent.GetComponent<HealthUnit>();
        }
        healthCtrl.TakeDamage(damage, transform.gameObject);
    }

    //private void OnDrawGizmos()
    //{
    //    Gizmos.color = Color.red;
    //    Gizmos.DrawWireCube(point, size);
    //}
}
