﻿using System;


public static class RandomSingleton
{
    static readonly Random rnd = new Random();

    public static Random getInstance()
    {
        if (rnd == null)
        {
            return new Random();
        }
        return rnd;
    }
}

