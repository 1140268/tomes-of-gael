﻿using UnityEngine;

public class Bow : Weapon, IRangedWeapon
{
    public Bow()
    {
        Name = "Bow";
        LightDmg = 5;
        HeavyDmg = 30;
        Length = 1;
        ChargeTime = 1;
        CastTime = 2;
        IsProjectile = true;
        MultiHits = false;
        ProjectileRange = 10;
        ProjectileSize = new UnityEngine.Vector2(1, 0.2f);
        ProjectileSpeed = 10;
        LoadProjectilePrefab("Arrow");
    }

    public void FixedMovement(ProjectileState state, Transform transform, Rigidbody2D rigidbody, Vector2 direction)
    {
        switch (state)
        {
            case ProjectileState.Moving:
                rigidbody.velocity = ProjectileSpeed * direction;
                break;
            case ProjectileState.Falling:
                rigidbody.bodyType = RigidbodyType2D.Dynamic;
                Vector2 currentVelocity = rigidbody.velocity;
                float angle = Mathf.Atan2(currentVelocity.y, currentVelocity.x) * Mathf.Rad2Deg;
                transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
                break;
            case ProjectileState.Hit:
                rigidbody.angularVelocity = 0;
                rigidbody.velocity = Vector2.zero;
                break;

        }
    }

    public override void Use()
    {
        throw new System.NotImplementedException();
    }
}
