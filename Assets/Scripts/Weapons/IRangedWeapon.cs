﻿using UnityEngine;

interface IRangedWeapon
{
    void FixedMovement(ProjectileState state, Transform transform, Rigidbody2D rigidbody, Vector2 direction);
}
