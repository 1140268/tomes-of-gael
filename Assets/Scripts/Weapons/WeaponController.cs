﻿using System.Collections.Generic;

public static class WeaponController
{
    private static Dictionary<string, Weapon> equippedWeapons = new Dictionary<string, Weapon>();
    private static Dictionary<string, Weapon> inventoryWeapons = new Dictionary<string, Weapon>();

    public static void AddToInventory(string idx, Weapon weapon)
    {
        inventoryWeapons.Add(idx, weapon);
    }

    public static void SetEquippedWeapon(string idx, Weapon weapon)
    {
        equippedWeapons.Add(idx, weapon);
    }

    public static void UseWeapon(string idx)
    {
        equippedWeapons[idx].Use();
    }

    public static Weapon GetInventoryWeapon(string idx)
    {
        return equippedWeapons[idx];
    }

    public static Weapon GetEquippedWeapon(string idx)
    {
        return equippedWeapons[idx];
    }
}