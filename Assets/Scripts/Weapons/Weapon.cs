﻿using UnityEngine;

public abstract class Weapon
{
    //Common
    public string Name { get; set; }
    public float LightDmg { get; set; }
    public float HeavyDmg { get; set; }
    public float Length { get; set; }
    public Vector2 LightSize { get; set; }
    public Vector2 HeavySize { get; set; }
    public float ChargeTime { get; set; }
    public float CastTime { get; set; }
    public bool MultiHits { get; set; }

    //Projectile
    public float ProjectileRange { get; set; }
    public float ProjectileSpeed { get; set; }
    public bool IsProjectile { get; set; }
    public Vector2 ProjectileSize { get; set; }
    public GameObject ProjectilePrefab { get; set; }

    public abstract void Use();

    protected void LoadProjectilePrefab(string name)
    {
        ProjectilePrefab = Resources.Load("Projectiles/" + name) as GameObject;
    }
}