﻿public class Spear : Weapon
{
    public Spear()
    {
        Name = "Spear";
        LightDmg = 15;
        HeavyDmg = 20;
        Length = 2;
        LightSize = new UnityEngine.Vector2(2, 1.5f);
        HeavySize = new UnityEngine.Vector2(3, 1);
        ChargeTime = 1.5f;
        CastTime = 1.5f;
        IsProjectile = false;
        MultiHits = false;
    }

    public override void Use()
    {
        throw new System.NotImplementedException();
    }
}
