﻿public class Knife : Weapon
{
    public Knife()
    {
        Name = "Knife";
        LightDmg = 5;
        HeavyDmg = 15;
        Length = 1;
        LightSize = new UnityEngine.Vector2(1, 1);
        HeavySize = new UnityEngine.Vector2(1, 1);
        Length = 1;
        ChargeTime = 0.5f;
        CastTime = 0.5f;
        IsProjectile = false;
        MultiHits = true;
    }

    public override void Use()
    {
        throw new System.NotImplementedException();
    }
}