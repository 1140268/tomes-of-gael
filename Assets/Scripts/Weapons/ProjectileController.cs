﻿using System.Collections;
using UnityEngine;

public class ProjectileController : MonoBehaviour
{
    //Public
    public Weapon weapon;
    public GameObject origin;
    public bool drawGizmos = false;
    public float damage = 0;
    public float facingDirection = 0;
    public LayerMask enemyMask;

    //Private
    [SerializeField] private float remainTime = 5;
    [SerializeField] private LayerMask groundMask;

    private Vector2 startPosition;
    private ProjectileState state;
    private Rigidbody2D m_rigidbody;
    private CapsuleCollider2D m_collider;
    private Vector2 velocity;
    private float speed;
    private Vector2 direction;
    private float distance;
    private Vector2 projectileSize;


    // Start is called before the first frame update
    void Start()
    {
        startPosition = transform.position;
        Collider2D[] targets = Physics2D.OverlapCircleAll(transform.position, weapon.ProjectileRange, enemyMask);
        Collider2D target = null;
        foreach (Collider2D item in targets)
        {
            if (Mathf.Sign(item.transform.position.x - transform.position.x) == facingDirection)
            {
                target = item;
                break;
            }
        }
        if (target != null)
        {
            RaycastHit2D targetCheck = Physics2D.Raycast(transform.position, (target.transform.position - transform.position).normalized, weapon.ProjectileRange, groundMask | enemyMask);
            direction = targetCheck.transform.CompareTag("Ground") ? (Vector2)transform.right * facingDirection : (targetCheck.point - (Vector2)transform.position).normalized;
        }
        else
        {
            direction = transform.right * facingDirection;
        }
        speed = weapon.ProjectileSpeed;
        distance = weapon.ProjectileRange;
        projectileSize = weapon.ProjectileSize;
        float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
        m_rigidbody = GetComponent<Rigidbody2D>();
        m_collider = GetComponent<CapsuleCollider2D>();
        gameObject.layer = origin.CompareTag("Player") ? 12 : 13;

        m_collider.size = projectileSize;
        state = ProjectileState.Moving;
    }

    // Update is called once per frame
    void Update()
    {
        if (state == ProjectileState.Moving && Vector2.Distance(startPosition, m_rigidbody.position) >= distance)
            {
                state = ProjectileState.Falling;
            }
    }

    private void FixedUpdate()
    {
        ((IRangedWeapon)weapon).FixedMovement(state, transform, m_rigidbody, direction);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (state != ProjectileState.Hit)
        {
            HealthUnit healthCtrl = collision.transform.parent.GetComponent<HealthUnit>();
            if (state != ProjectileState.Falling && healthCtrl != null)
            {
                healthCtrl.TakeDamage(damage, origin);
            }
            state = ProjectileState.Hit;
            m_rigidbody.bodyType = RigidbodyType2D.Kinematic;
            m_collider.enabled = false;
            transform.SetParent(collision.transform.parent, true);
            StartCoroutine(Cleanup(remainTime));
        }
    }

    IEnumerator Cleanup(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        Destroy(gameObject);
    }

    private void OnDrawGizmos()
    {
        if (drawGizmos)
        {
            Gizmos.color = Color.red;
            Gizmos.DrawWireCube(m_collider.bounds.center, m_collider.bounds.size);
        }
    }
}

public enum ProjectileState
{
    Moving,
    Falling,
    Hit
}
