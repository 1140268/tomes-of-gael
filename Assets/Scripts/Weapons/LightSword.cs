﻿public class LightSword : Weapon
{
    public LightSword()
    {
        Name = "LightSword";
        LightDmg = 10;
        HeavyDmg = 30;
        Length = 1.5f;
        LightSize = new UnityEngine.Vector2(1.5f, 1);
        HeavySize = new UnityEngine.Vector2(1, 2);
        ChargeTime = 1;
        CastTime = 1;
        IsProjectile = false;
        MultiHits = true;
    }

    public override void Use()
    {
        throw new System.NotImplementedException();
    }
}
