﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

public static  class WeaponFactory
{
    private static Dictionary<string, Type> weaponsByName;
    
    private static void Initialize()
    {
        if (weaponsByName != null)
            return;

        IEnumerable<Type> weaponTypes = Assembly.GetAssembly(typeof(Weapon)).GetTypes()
            .Where(type => type.IsClass && !type.IsAbstract && type.IsSubclassOf(typeof(Weapon)));

        weaponsByName = new Dictionary<string, Type>();

        foreach (Type subType in weaponTypes)
        {
            Weapon weapon = Activator.CreateInstance(subType) as Weapon;
            weaponsByName.Add(weapon.Name, subType);
        }
    }

    public static Weapon GetWeaponByName(string name)
    {
        Initialize();
        if (weaponsByName.ContainsKey(name))
        {
            Type type = weaponsByName[name];
            Weapon weapon = Activator.CreateInstance(type) as Weapon;
            return weapon;
        }
        return null;
    }

    public static IEnumerable<string> GetWeaponNames()
    {
        Initialize();
        return weaponsByName.Keys;
    }
}
