﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeadlyGround : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        HealthUnit healthCtrl = collision.transform.parent.GetComponent<HealthUnit>();
        if (healthCtrl != null)
        {
            healthCtrl.TakeDamage(float.MaxValue);
        }
    }
}
