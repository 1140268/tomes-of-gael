﻿using System.Collections.Generic;
using UnityEngine;

public class PoisonHazard : MonoBehaviour
{
    [SerializeField] private float damage = 10;
    [SerializeField] private float rate = 1;
    [SerializeField] private float delay = 1;
    private Dictionary<string, PoisonData> queue = new Dictionary<string, PoisonData>();

    struct PoisonData
    {
        public HealthUnit healthCtrl;
        public float nextTick;

        public PoisonData(HealthUnit healthCtrl, float timerInit)
        {
            this.healthCtrl = healthCtrl;
            nextTick = timerInit;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.transform.parent == null) return;
        HealthUnit healthCtrl = collision.transform.parent.GetComponent<HealthUnit>();
        if (healthCtrl != null)
        {
            queue.Add(collision.name, new PoisonData(healthCtrl, Time.time + delay));
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (queue.TryGetValue(collision.name, out PoisonData data) && Time.time >= data.nextTick)
        {
            data.healthCtrl.TakeDamage(damage, isTargetable: false);
            data.nextTick = Time.time + rate;
            queue[collision.name] = data;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        queue.Remove(collision.name);
    }
}
