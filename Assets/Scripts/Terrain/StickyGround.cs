﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StickyGround : MonoBehaviour
{
    [SerializeField] bool isSticky = false;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (isSticky)
        {
            if (collision.CompareTag("Player"))
            {
                collision.transform.parent.GetComponent<PlayerController>().GetStuck();
            }
        }
    }
}
