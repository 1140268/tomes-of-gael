﻿using System;
using UnityEngine;

public class SpikeHazard : MonoBehaviour
{
    [SerializeField] private float damage = 20;
    [SerializeField] private Vector2 pushForce;
    private void OnCollisionEnter2D(Collision2D collision)
    {
        HealthUnit healthCtrl = collision.transform.GetComponent<HealthUnit>();
        Rigidbody2D target = collision.transform.GetComponent<Rigidbody2D>();
        if(target != null)
        {
            Vector2 collisionDir = collision.contacts[0].point - (Vector2)transform.position;
            Vector2 dir = new Vector2(Mathf.Cos(Mathf.Deg2Rad * 60) * Mathf.Sign(collisionDir.x), Mathf.Sin(Mathf.Deg2Rad * 60));
            dir = dir.normalized;
            healthCtrl.AddStatus(new PushStatus(target, dir * pushForce, collision.collider.GetComponent<GroundCheck>().check));
        }
        if (healthCtrl != null)
        {
            healthCtrl.TakeDamage(damage);
        }
    }
}
