﻿using UnityEngine;

public class MovingPlatformController : MonoBehaviour
{
    //Public
    public float moveSpeed = 3;
    public Transform destination;
    public Transform physicalBlock;

    //Private
    private Vector2 startPos;
    private Vector2 finalPos;
    private Vector2 nextPos;

    // Start is called before the first frame update
    void Start()
    {
        startPos = physicalBlock.position;
        finalPos = destination.position;
        nextPos = finalPos;
    }

    // Update is called once per frame
    void Update()
    {
        Movement();
    }

    private void Movement()
    {
        Vector2 direction = nextPos - (nextPos == startPos ? finalPos : startPos);
        physicalBlock.position += new Vector3(direction.normalized.x * moveSpeed * Time.deltaTime, direction.normalized.y * moveSpeed * Time.deltaTime);
        if (Vector3.Distance(physicalBlock.position, nextPos) <= 0.1)
        {
            nextPos = nextPos == startPos ? finalPos : startPos;
        }

    }
}
