﻿using System;
using UnityEngine;

public abstract class Status
{
    public float Duration { set; get; }
    public bool Active { set; get; }
    public bool IsBlock { set; get; }

    public Status(float duration)
    {
        Duration = Time.time + duration;
        Active = true;
    }

    public abstract void Tick();

    public void IncrementDuration(float seconds)
    {
        Duration += seconds;
    }

    public bool HasTimeExpired()
    {
        return Time.time >= Duration;
    }
}