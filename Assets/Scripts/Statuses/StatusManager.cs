﻿using System.Collections.Generic;

public class StatusManager
{
    private List<Status> _statuses = new List<Status>();
    private int blockingStatuses = 0;
    public void AddStatus(Status status)
    {
        if (status.IsBlock)
        {
            blockingStatuses++;
        }
        _statuses.Add(status);
    }

    public void Tick()
    {
        List<Status> temp = new List<Status>(_statuses);
        foreach (Status status in temp)
        {
            if (status.HasTimeExpired() || !status.Active)
            {
                if (status.IsBlock)
                {
                    blockingStatuses--;
                }
                _statuses.Remove(status);
            }
            else
            {
                status.Tick();
            }
        }
    }

    public bool IsBlocking()
    {
        return blockingStatuses > 0;
    }

    public void ClearStatuses()
    {
        _statuses.Clear();
    }
}