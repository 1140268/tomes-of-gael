﻿using System;
using UnityEngine;

public class PushStatus : Status
{
    Rigidbody2D body;
    Vector2 force;
    Func<Collider2D> groundCheck;
    bool _pushing;
    bool _jumped;

    public PushStatus(Rigidbody2D body, Vector2 force, Func<Collider2D> groundCheck) : base(float.MaxValue)
    {
        IsBlock = true;
        this.force = force;
        this.body = body;
        this.groundCheck = groundCheck;
        _pushing = true;
        _jumped = false;
    }

    public override void Tick()
    {
        if (_pushing)
        {
            body.AddForce(force);
            _pushing = false;
        }
        
        if(_jumped && groundCheck())
        {
            Active = false;
        }

        if(!_jumped && !groundCheck())
        {
            _jumped = true;
        }
    }
}